FROM ubuntu
MAINTAINER Radu Graur <radu.graur@gmail.com>

# Install PHP-FPM
RUN apt-get update && apt-get -y install php5-fpm php5-cli php5-mysqlnd php5-mcrypt php5-redis php5-gd

# Configure PHP-FPM
RUN sed -i "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php5/fpm/php-fpm.conf && \
    sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php5/fpm/php.ini && \
    sed -i "s/^listen =.*/listen = 9000/g" /etc/php5/fpm/pool.d/www.conf

# Cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /opt/data
VOLUME ["/opt/data"]
WORKDIR /opt/data

EXPOSE 9000
CMD ["php5-fpm", "-F"]

