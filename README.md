# PHP-FPM Docker image #

### Base image ###
* official [Ubuntu image](https://registry.hub.docker.com/_/ubuntu/)

### Installed packages ###
* php5-fpm

### Specifications ###
* forwarded port: 9000

### Run ###
* build image: `docker build -t phpfpm .`
* create container: `docker run -p 9000:9000 --name phpfpm -d phpfpm`

